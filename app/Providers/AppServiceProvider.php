<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(\Laravel\Tinker\TinkerServiceProvider::class);

        $this->app->register(\Illuminate\Redis\RedisServiceProvider::class);

        $this->app->register(\Illuminate\Filesystem\FilesystemServiceProvider::class);

        $this->app->register(\Illuminate\Mail\MailServiceProvider::class);
        $this->app->alias('mail.manager', \Illuminate\Mail\MailManager::class);
        $this->app->alias('mail.manager', \Illuminate\Contracts\Mail\Factory::class);
        $this->app->alias('mailer', \Illuminate\Mail\Mailer::class);
        $this->app->alias('mailer', \Illuminate\Contracts\Mail\Mailer::class);
        $this->app->alias('mailer', \Illuminate\Contracts\Mail\MailQueue::class);

        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
