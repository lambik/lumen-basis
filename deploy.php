<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', '<my_project_name>');

// Project repository
set('repository', 'git@github.com:<path_to_repository>.git');
//set('repository', 'git@bitbucket.org:<path_to_repository>.git');

// [Optional] Allocate tty for git clone. Default value is false.
//set('git_tty', true);
//set ('ssh_multiplexing', false);

/**
 * Make sure the deploy host has access to the github/bitbucket server via ssh and a deployment key!
 */

// Hosts
host('production')
    ->hostname('ip-address or hostname')
    ->user('username')
    ->identityFile('./private-key.pem') // this is a local private key to log in the deploy host, which is separate from the deployment key!
    ->set('deploy_path', '/path/on/server/{{application}}')
    ->set('branch', 'master')
;


// Tasks
task('build', function () {
    run('cd {{release_path}} && build');
});
task('npm:mix', function () {
    run('cd {{release_path}} && npm ci && npm run production');
});

// overwrite the default deployer command for lumen installations
// empty functions mean: nothing to do, there is no such thing in lumen
/*
task('artisan:storage:link', function() {
    run('ln -s {{release_path}}/storage/app/public {{release_path}}/public/storage');
});
task('artisan:view:cache', function() {});
task('artisan:config:cache', function() {});
task('artisan:optimize', function() {});
*/

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Mix the css/js before symlink
// before('deploy:symlink', 'npm:mix');

// Notify the queue workers before symlink
before('deploy:symlink', 'artisan:queue:restart');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');
